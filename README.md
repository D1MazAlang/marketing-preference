#Marketing Preference
Exposes two methods POST and PUT to create and update market preference. The GET method was purely created for marketing-preference-info micorservice and for testing purposes.
Uses embedded H2 database to store records once POST marketing preference is invoked.

##Pre-requisite
JDK 8
Gradle 7.1.1
Docker 3.6.0

##Build Gradle

1.  Download or clone from bitbucket repository
2.  If downloaded on upper right next to clone. Click on ... and Download Repository to selected directory
3.  If cloned from bitbucket 
    *   Open terminal and paste command git clone https://D1MazAlang@bitbucket.org/D1MazAlang/marketing-preference.git
4.  Once the source is saved locally run gradle clean build
5.  Run the jar either gradle bootRun or java -jar build/lib/marketing-preference-0.0.1-SNAPSHOT.jar

##Run Docker

Ensure docker is installed on your local system

1.  Execute the command `docker build --build-arg JAR_FILE=build/libs/marketing-preference-0.0.1-SNAPSHOT.jar -t marketing-preference:latest .`
2.  Check if docker image is created `docker image ls`
3.  Initial Run for newly docker image `docker run --name marketing-preference -d -p 9191:9191 marketing-preference:latest --server.port=9191`
4.  Verify if its running `docker ps`, where a record should show up for marketing-preference
5.  Verify by entering the site on browser `http://localhost:9191/v1/marketing/preference/greet`
6.  Stop docker image 
    * `docker ps`
    * Get the alphanumeric value under Container ID column
    * execute `docket stop container_id`
    * Verify if process still exists `docker ps` should not be visible for marketing-preference
    
##Testing Marketing-Preference Microservice

###Test Create Marketing Preference

1.  Open postman and click New
2.  Change HTTP Type to POST
3.  Enter URL `http://localhost:9191/v1/marketing/preference`
4.  Click Body and check raw and changed drop down from text to JSON(application/json)
5.  Paste the JSON string 
`{
    "id": "1",
    "customerName" : "Test1",
    "customerPreference" : "sms"
    }`
6.  Successful result should return the JSON response and 200 OK
7.  Any values other than SMS, POST or EMAIL assigned to customerPreference will result to validation error and will not update / register preference

###Test Update Marketing Preference

1.  Open postman and click New
2.  Change HTTP Type to PUT
3.  Enter URL `http://localhost:9191/v1/marketing/preference/{id}` replace id with the number previously registered
4.  Click Body and check raw and changed drop down from text to JSON(application/json)
5.  Paste the JSON string. Only Customer Preference will be updated! 
    `{
    "id": "1",
    "customerName" : "Test1",
    "customerPreference" : "email"
    }`
6.  Successful result should return the JSON response and 200 OK
7.  Any values other than SMS, POST or EMAIL assigned to customerPreference will result to validation error and will not update / register preference


###Test GET Marketing Preference record

The original requirement only Create and Update Preference were required. The service to retrieve marketing preference is purely a utility service meant to be used by marketing-preference-info microservice and for testing to verify the records has been inserted.
1.  Open postman and click New
2.  Change HTTP Type to GET
3.  Enter URL `http://localhost:9191/v1/marketing/preference/{id}` replace id with the number previously registered
4.  JSON body should be returned with 200 OK