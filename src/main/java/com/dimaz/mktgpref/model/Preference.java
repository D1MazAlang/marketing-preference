package com.dimaz.mktgpref.model;

public class Preference {

	String preference;

	public String getPreference() {
		return preference;
	}

	public void setPreference(String preference) {
		this.preference = preference;
	}
	
}
