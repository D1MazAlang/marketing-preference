package com.dimaz.mktgpref.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dimaz.mktgpref.model.Customer;

@Repository
public interface PreferenceRepository extends JpaRepository<Customer, Long>{

}
