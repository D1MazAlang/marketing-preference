package com.dimaz.mktgpref;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketingPreferenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarketingPreferenceApplication.class, args);
	}

}
