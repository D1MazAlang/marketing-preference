package com.dimaz.mktgpref.endpoint;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dimaz.mktgpref.model.Customer;
import com.dimaz.mktgpref.repository.PreferenceRepository;
import com.dimaz.mktgpref.service.PreferenceValidation;

import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController()
@RequestMapping("/v1")
public class PreferenceRegistry {

	PreferenceRepository repo;
	PreferenceValidation pref;
	
	@Autowired
	public PreferenceRegistry(PreferenceRepository repo, PreferenceValidation pref) {
		this.repo = repo;
		this.pref = pref;
	}
	
	@GetMapping("/marketing/preference/greet")
	public ResponseEntity<String> greetings() {
		return new ResponseEntity<String>("Welcome to Marketing Preference Microservice - Register Your Marketing Preference", HttpStatus.OK);
	}
	
	@PostMapping("/marketing/preference")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Customer> registerPreference(@RequestBody(required = true) Customer cust ){
		log.info("Register Customer Marketing Preference");
			
		if ( pref.validateCustomerPreference(cust) ) {
			log.debug("Customer Validated... save preference in DB");
			
			return new ResponseEntity<Customer>(repo.save(cust), HttpStatus.OK);
						
		}else {
			log.debug("Customer not validated ");
			return new ResponseEntity<Customer>(cust, HttpStatus.EXPECTATION_FAILED);
		}
			
	}
	
	@PutMapping("/marketing/preference/{id}")
	public ResponseEntity<Customer> custMarketingPreference(@PathVariable(name = "id", required = true) 
															Long id, @RequestBody(required = true) Customer cust) throws NotFoundException {
		
		log.info("Update Customer's Marketing Preference " );
		
		Optional<Customer> custData = repo.findById(id); 
		
		Customer updCust = custData.orElseThrow( () -> 
					new NotFoundException("Record ID " + id + " not found ") );
		
		//Update only the marketing preference
		updCust.setCustomerPreference(cust.getCustomerPreference());
		
		log.debug("Valid and Update id: "+ id );
		
		if ( pref.validateCustomerPreference(cust) ) {
			log.debug("Customer Validated... save preference in DB");
			
			return new ResponseEntity<Customer>(repo.save(updCust), HttpStatus.OK);
						
		}else {
			log.debug("Customer not validated ");
			return new ResponseEntity<Customer>(updCust, HttpStatus.NOT_MODIFIED);
		}
	}
	
	@GetMapping("/marketing/preference/{id}")
	public ResponseEntity<Customer> getCustomerMarketingPref(@PathVariable("id") Long id) {
		Optional<Customer> custData = repo.findById(id);
		
		log.info("Retrieve Customer Marketing Preference");
		
		if (custData.isPresent()) {
			return new ResponseEntity<Customer>(custData.get(), HttpStatus.OK);
		}else {
			return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
		}
	}
}
