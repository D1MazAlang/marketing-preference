package com.dimaz.mktgpref.service;

import org.springframework.stereotype.Service;

import com.dimaz.mktgpref.model.Customer;

@Service
public class PreferenceValidation {

	public boolean validateCustomerPreference(Customer cust) {
		
		boolean isValid = false;
		
		if ("SMS".equalsIgnoreCase(cust.getCustomerPreference()) ) {
			isValid = true;
		}else if ("POST".equalsIgnoreCase(cust.getCustomerPreference()) ) {
			isValid = true;
		}else if ("EMAIL".equalsIgnoreCase(cust.getCustomerPreference()) ) {
			isValid = true;
		}else {
			isValid = false;
		}
		
		return isValid;
	}
	
}
