DROP TABLE IF EXISTS Customer;

CREATE TABLE Customer (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  customer_name VARCHAR(100) NOT NULL,
  customer_preference VARCHAR(20) NOT NULL

);